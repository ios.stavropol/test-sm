import React, { useEffect, useRef } from 'react';
import {Platform, Image, View, Text, TouchableOpacity, ActivityIndicator, Animated, Easing} from 'react-native';
import { useRoute } from '@react-navigation/native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from '../../utilities/Common';
import { colors } from '../../styles';

const UserView = ({data, onClick}) => {

	const [openView, setOpenView] = React.useState(false);

	return (<TouchableOpacity style={{
		width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
		marginTop: Common.getLengthByIPhone7(8),
		borderRadius: Common.getLengthByIPhone7(12),
		padding: Common.getLengthByIPhone7(10),
		backgroundColor: 'white',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.08,
		shadowRadius: 7.00,
		elevation: 1,
	}}
	onPress={() => {
		if (onClick) {
			onClick();
		}
	}}>
		<Text style={{
			color: colors.TEXT_COLOR,
			fontFamily: 'Gilroy-Regular',
			fontWeight: 'normal',
			textAlign: 'left',
			fontSize: Common.getLengthByIPhone7(14),
			lineHeight: Common.getLengthByIPhone7(17),
		}}
		numberOfLines={1}
		allowFontScaling={false}>
			{data.name}
		</Text>
		<Text style={{
			color: colors.TEXT_COLOR,
			fontFamily: 'Gilroy-Regular',
			fontWeight: 'normal',
			textAlign: 'left',
			fontSize: Common.getLengthByIPhone7(10),
			lineHeight: Common.getLengthByIPhone7(12),
		}}
		allowFontScaling={false}>
			{data.email}
		</Text>
	</TouchableOpacity>);
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
    
});

export default connect(mstp, mdtp)(UserView);