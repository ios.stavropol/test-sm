/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { createRef, MutableRefObject, useEffect } from 'react';
import {
  CommonActions,
  NavigationContainerRef,
} from '@react-navigation/native';

export const isMountedRef = createRef<boolean>();

export const navigationRef = createRef<NavigationContainerRef>();

export function useNavigationMounting() {
  useEffect(() => {
    (isMountedRef as MutableRefObject<boolean>).current = true;

    return () => {
      (isMountedRef as MutableRefObject<boolean>).current = false;
    };
  }, []);
}

export function navigate<T>(name: string, params?: T) {
  if (isMountedRef.current && navigationRef.current) {
    navigationRef.current.navigate(name, params as unknown as object);
  } else {
    console.warn('Navigation not mounted. Cannot navigate to:', name);
  }
}

export function resetRoot(routeName: string, params?: object) {
  if (isMountedRef.current && navigationRef.current) {
    navigationRef.current.resetRoot({
      index: 0,
      routes: [{ name: routeName, params }],
    });
  } else {
    console.warn('Navigation not mounted. Cannot reset root to:', routeName);
  }
}

export function setParams(params: object, routeKey?: string) {
  if (isMountedRef.current && navigationRef.current) {
    navigationRef.current.dispatch({
      ...CommonActions.setParams(params),
      source: routeKey,
    });
  } else {
    console.warn('Navigation not mounted. Cannot set params to:', routeKey);
  }
}

export function canGoBack() {
  if (!isMountedRef.current || !navigationRef.current) return false;

  return navigationRef.current.canGoBack();
}

export function goBack() {
  if (!isMountedRef.current || !navigationRef.current) return false;

  return navigationRef.current.goBack();
}

export function getRootState() {
  if (!isMountedRef.current || !navigationRef.current) return false;

  return navigationRef.current.getRootState();
}

export function getCurrentRoute() {
  if (!isMountedRef.current || !navigationRef.current) return false;

  return navigationRef.current.getCurrentRoute();
}

export function isFocused() {
  if (!isMountedRef.current || !navigationRef.current) return false;

  return navigationRef.current.isFocused;
}
