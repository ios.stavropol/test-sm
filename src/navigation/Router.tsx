import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import { Image, Platform } from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import { isIphoneX } from 'react-native-iphone-x-helper';
import { colors } from '../styles';
import { typography } from '../styles';
import MainScreen from '../screens/Main';
import UserScreen from '../screens/User';

import common from './../utilities/Common';

export const Stack = createNativeStackNavigator();

export default Router = () => {
  return (
    <NavigationContainer>
      <SafeAreaProvider>
        <Stack.Navigator>
          <Stack.Screen
            name={'Main'}
            component={MainScreen}
            options={{
                
            }}
          />
          <Stack.Screen
            name={'User'}
            component={UserScreen}
            options={{
                
            }}
          />
        </Stack.Navigator>
      </SafeAreaProvider>
    </NavigationContainer>
  );
}
