import React, { useCallback, useEffect, useRef } from 'react';
import { Platform, View, FlatList, TouchableOpacity, Text, StatusBar } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import UserView from '../../components/Main/UserView';

const MainScreen = ({getUsers, users, setCurrentUser}) => {

	const navigation = useNavigation();

	useEffect(() => {
		getUsers()
		.then(() => {

		})
		.catch(err => {

		});
	}, []);

	const renderRow = (item: object, index: number) => {
		return (<UserView
			data={item}
			index={index}
			onClick={() => {
				setCurrentUser(item);
				navigation.navigate('User', {data: item});
			}}
		/>);
    }

	return (
		<View style={{
			flex: 1,
			backgroundColor: colors.BACKGROUND_COLOR,
			alignItems: 'center',
			justifyContent: 'space-between',
		}}>
			<FlatList
				style={{
					flex: 1,
					width: Common.getLengthByIPhone7(0),
				}}
				contentContainerStyle={{
					alignItems: 'center',
					justifyContent: 'flex-start',
				}}
				bounces={true}
				removeClippedSubviews={false}
				scrollEventThrottle={16}
				data={users}
				extraData={users}
				keyExtractor={(item, index) => index.toString()}
				renderItem={({item, index}) => renderRow(item, index)}
			/>
		</View>
	);
};

const mstp = (state: RootState) => ({
	users: state.user.users,
});

const mdtp = (dispatch: Dispatch) => ({
	getUsers: () => dispatch.user.getUsers(),
	setCurrentUser: (payload) => dispatch.user.setCurrentUser(payload),
});

export default connect(mstp, mdtp)(MainScreen);
