//@ts-nocheck
import { createModel } from '@rematch/core';
import { API, StorageHelper } from './../../services';
import { ErrorsHelper } from './../../helpers';
import { Dispatch } from 'store';
import type { RootModel } from './../models';
import AsyncStorage from '@react-native-async-storage/async-storage';

type ButtonState = {
	skipName: any,
};

const buttons = createModel<RootModel>()({
	state: {
		skipName: null,
	} as ButtonState, 
	reducers: {
		setSkipName: (state, payload: boolean) => ({
			...state,
			skipName: payload,
		}),
	},
	effects: (dispatch) => {
		return {
			// showToastMessage(text) {
			// 	dispatch.buttons.setToastText(text);
			// 	dispatch.buttons.setShowToast(true);
			// },
		}
	},
});

export default buttons;