//@ts-nocheck
import { createModel } from '@rematch/core';
import { API, StorageHelper } from './../../services';
import { ErrorsHelper } from './../../helpers';
import { Dispatch } from 'store';
import type { RootModel } from './../models';
import { BASE_URL } from './../../constants';
import Common from './../../utilities/Common';

type UserState = {
	isRequestGoing: false,
	users: [],
	currentUser: null,
};

const user = createModel<RootModel>()({
	state: {
		isRequestGoing: false,
		users: [],
		currentUser: null,
	} as UserState, 
	reducers: {
		setRequestGoingStatus: (state, payload: boolean) => ({
			...state,
			isRequestGoing: payload,
		}),
		setUsers: (state, payload: object) => ({
			...state,
			users: payload,
		}),
		setCurrentUser: (state, payload: object) => ({
			...state,
			currentUser: payload,
		}),
	},
	effects: (dispatch) => {
		return {
			async getUsers() {
				return new Promise((resolve, reject) => {
					dispatch.user.setRequestGoingStatus(true);
					API.common.getUsers()
					.then(response => {
						console.warn('getUsers -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							dispatch.user.setUsers(response.data);
							resolve();
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						if (err.response.status == 400) {
							console.warn(err.response);
							reject(err.response.data.error);
						} else {
							reject('Произошла неизвестная ошибка! Повторите снова.');
						}
					});
				});
			},
		}
	},
});

export default user;