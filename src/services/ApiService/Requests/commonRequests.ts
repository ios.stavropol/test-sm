import axios from 'axios';
import { BASE_URL } from './../../../constants';

class CommonAPI {
	getUsers = () => {
		return axios.get(BASE_URL + '/users');
	};
}

const commonRequests = new CommonAPI();
export default commonRequests;
