import React, { useEffect } from 'react';
import Router from './src/navigation/Router';
import {StatusBar, Platform} from 'react-native';
import useAppState from 'react-native-appstate-hook';
import { Provider } from 'react-redux';
import { theme } from './theme';
import { ThemeProvider } from 'styled-components';
import store from './src/store';
import LoadingView from './src/components/LoadingView';
import { colors } from './src/styles';

const App = () => {

  useEffect(() => {
    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('light-content', true);
    } else {
      StatusBar.setBarStyle('light-content', true);
      // StatusBar.setBackgroundColor(colors.BLUE_COLOR, true);
    }
  }, []);

  useAppState({
    onChange: newAppState => {
      console.warn(newAppState);
    },
    onForeground: () => {
      
    },
    onBackground: () => console.warn('App went to background'),
  });

  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <Router />
      </ThemeProvider>
      <LoadingView />
    </Provider>
  );
};

export default App;
